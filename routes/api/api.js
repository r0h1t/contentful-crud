
const express = require("express");
const Joi = require("@hapi/joi");
const router = express.Router();
const contentful = require('contentful-management')



// Access Token -- Content Management API token
const client = contentful.createClient({
    accessToken: 'CFPAT-kDIjY_JXf7vy4dKc3uSAzyZZl0UaA5ACK_ZWcYJ5PmA'
});



router.get('/', (req,res) => {

    res.send("Welcome to Contentful CMS via API");
});

router.get('/:spaceid/:environment/:contenttypeid',(req,res) => {

    const methodName = "Get Content with ID";

       
    client.getSpace(req.params.spaceid)
    .then((space) => space.getEnvironment(req.params.environment))
    .then((environment) => environment.getContentType(req.params.contenttypeid))
    .then((contentType) =>  res.send(contentType))
    .catch((error) =>  {console.error(`Error in method : ${methodName} `,error);res.send(error)})

});

router.post('/',(req,res) => {

  
    const {error, value} = validateJson(req.body);

    if(error)
    {
        console.log("EROOR IN VALIDATION");
        console.log(error.details[0].message);
        res.send(error.details[0].message); 
        return;
    }
    
   

    console.log('spaceid ID:', req.body.spaceid);
    console.log('environment ID:', req.body.environment);
    console.log('contenttypeid ID:', req.body.contenttypeid);
    

     client.getSpace(req.body.spaceid)
        .then((space) => space.getEnvironment(req.body.environment))
        .then((environment) => environment.createContentTypeWithId(req.body.contenttypeid, 
            req.body.data))
        .then((contenttypeid) =>  res.send(contenttypeid))
        .catch((error) =>  {console.error("ERROR--->",error);res.send(error)})
        

    
});


router.put('/:contenttypeid',(req,res) => {
      
    const {error, value} = validateJson(req.body);
    if(error)
    {
        
        console.log("Validation Error -->",error.details[0].message);
        res.send(error.details[0].message);
        return;
    }

    client.getSpace(req.body.spaceid)
    .then((space) => space.getEnvironment(req.body.environment))
    .then((environment) => environment.getContentType(req.params.contenttypeid))
    .then((contentType) => {
        console.log("CONTENT TYPE-->",contentType.fields);
        contentType.fields.push(req.body.update);

     return contentType.update()
    })
    .then((contentType) =>  res.send(contentType))
    .catch((error) =>  {console.error("ERROR--->",error);res.send(error)})

});

router.delete('/:spaceid/:environment/:contenttypeid', (req,res)=> {
    
    client.getSpace(req.params.spaceid)
    .then((space) => space.getEnvironment(req.params.environment))
    .then((environment) => environment.getContentType(req.params.contenttypeid))
    .then((contentType) => contentType.unpublish())
    .then((contentType) => contentType.delete())
    .then((contentType) =>  res.send(contentType))
    .catch((error) =>  {console.error("ERROR--->",error);res.send(error)})
       
});


function validateJson (inputJson) {
    console.log(inputJson);

    const fieldDataSchema = Joi.object();
    const fieldsSchema = Joi.array().items(fieldDataSchema);
    const dataSchema = Joi.object({
        name: Joi.string(),
        fields: fieldsSchema
    })

    const schema = Joi.object({
        contenttypeid: Joi.string().min(3).required(),
        environment: Joi.string().min(3),
        spaceid: Joi.string(),
        update: Joi.object(),
        data: dataSchema        
    });
  
    return schema.validate(inputJson);

}

module.exports = router;